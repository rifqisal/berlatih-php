<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number >= 85 && $number <= 100) {
        echo "Nilai: Sangat Baik <br>";
    } else if ($number >= 70 && $number < 85) {
        echo "Nilai: Baik <br>";
    } else if ($number >= 60 && $number < 70) {
        echo "Nilai: Cukup <br>";
    } else {
        echo "Nilai: Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang